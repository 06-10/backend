package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Address;
import com.devcamp.api.repository.AddressRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class AddressController {
    
    @Autowired
    AddressRepository addressRepository;

    //GET ALL
    @GetMapping("/address")
    public ResponseEntity<List<Address>> getAllAddress(){
        try {
            List<Address> allAddress = new ArrayList<>();
            addressRepository.findAll().forEach(allAddress::add);
            return new ResponseEntity<>(allAddress,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/address/{id}")
    public ResponseEntity<Address> getAddressById(@PathVariable("id") int id){
        try {
            Optional<Address> findAddress = addressRepository.findById(id);
            Address addressData = findAddress.get();
            return new ResponseEntity<Address>(addressData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/address")
    public ResponseEntity<Object> createAddress(@Valid @RequestBody Address pAddress){
        try {
            Address newAddress = new Address();
            newAddress.setAddress(pAddress.getAddress());
            newAddress.setLat(pAddress.getLat());
            newAddress.setLng(pAddress.getLng());

            Address saveAddress = addressRepository.save(newAddress);
            return new ResponseEntity<>(saveAddress,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/address/{id}")
    public ResponseEntity<Object> updateAddress(@RequestBody Address pAddress,@PathVariable("id") int id){
        try {
            Optional<Address> findAddress = addressRepository.findById(id);
            if(findAddress.isPresent()){
                Address addressData = findAddress.get();
                addressData.setAddress(pAddress.getAddress());
                addressData.setLat(pAddress.getLat());
                addressData.setLng(pAddress.getLng());

                Address saveAddress = addressRepository.save(addressData);
                return new ResponseEntity<>(saveAddress,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/address/{id}")
    public ResponseEntity<Address> deleteAddressById(@PathVariable("id") int id){
        try {
            Optional<Address> findAddress = addressRepository.findById(id);
            if(findAddress.isPresent()){
                addressRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
