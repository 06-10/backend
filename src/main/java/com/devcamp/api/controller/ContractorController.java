package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Contractor;
import com.devcamp.api.repository.ContractorRepository;


@RestController
@CrossOrigin
@RequestMapping("/")
public class ContractorController {
    
    @Autowired
    ContractorRepository contractorRepository;

    //GET ALL
    @GetMapping("/contractor")
    public ResponseEntity<List<Contractor>> getAllAddress(){
        try {
            List<Contractor> allContractors = new ArrayList<>();
            contractorRepository.findAll().forEach(allContractors::add);
            return new ResponseEntity<>(allContractors,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/contractor/{id}")
    public ResponseEntity<Contractor> getAddressById(@PathVariable("id") int id){
        try {
            Optional<Contractor> findContractor = contractorRepository.findById(id);
            Contractor contractorData = findContractor.get();
            return new ResponseEntity<Contractor>(contractorData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/contractor")
    public ResponseEntity<Object> createContructor(@RequestBody Contractor pContractor){
        try {
            Contractor newContractor = new Contractor();
            newContractor.setAddress(pContractor.getAddress());
            newContractor.setDescription(pContractor.getDescription());
            newContractor.setEmail(pContractor.getEmail());
            newContractor.setFax(pContractor.getFax());
            newContractor.setName(pContractor.getName());
            newContractor.setNote(pContractor.getNote());
            newContractor.setPhone(pContractor.getPhone());
            newContractor.setPhone2(pContractor.getPhone2());
            newContractor.setProjects(pContractor.getProjects());
            newContractor.setWebsite(pContractor.getWebsite());

            Contractor saveContractor = contractorRepository.save(newContractor);
            return new ResponseEntity<>(saveContractor,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/contractor/{id}")
    public ResponseEntity<Object> updateContractor(@Valid @RequestBody Contractor pContractor,@PathVariable("id") int id){
        try {
            Optional<Contractor> findContractor = contractorRepository.findById(id);
            if(findContractor.isPresent()){
                Contractor contractorData = findContractor.get();
                contractorData.setAddress(pContractor.getAddress());
                contractorData.setDescription(pContractor.getDescription());
                contractorData.setEmail(pContractor.getEmail());
                contractorData.setFax(pContractor.getFax());
                contractorData.setName(pContractor.getName());
                contractorData.setNote(pContractor.getNote());
                contractorData.setPhone(pContractor.getPhone());
                contractorData.setPhone2(pContractor.getPhone2());
                contractorData.setProjects(pContractor.getProjects());
                contractorData.setWebsite(pContractor.getWebsite());

                Contractor saveContractor = contractorRepository.save(contractorData);
                return new ResponseEntity<>(saveContractor,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/contractor/{id}")
    public ResponseEntity<Contractor> deleteContractorById(@PathVariable("id") int id){
        try {
            Optional<Contractor> findContractor = contractorRepository.findById(id);
            if(findContractor.isPresent()){
                contractorRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
