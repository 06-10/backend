package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Date;

import com.devcamp.api.model.Customer;
import com.devcamp.api.repository.CustomerRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {
    
    @Autowired
    CustomerRepository customerRepository;

    //GET ALL
    @GetMapping("/customer")
    public ResponseEntity<List<Customer>> getAllCustomers(){
        try {
            List<Customer> allCustomers = new ArrayList<>();
            customerRepository.findAll().forEach(allCustomers::add);
            return new ResponseEntity<>(allCustomers,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/customer/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable("id") int id){
        try {
            Optional<Customer> findCustomer = customerRepository.findById(id);
            Customer customerData = findCustomer.get();
            return new ResponseEntity<Customer>(customerData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/customer")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer pCustomer){
        try {
            Customer newCustomer = new Customer();
            newCustomer.setAddress(pCustomer.getAddress());
            newCustomer.setContactName(pCustomer.getContactName());
            newCustomer.setContactTitle(pCustomer.getContactTitle());
            newCustomer.setMobile(pCustomer.getMobile());
            newCustomer.setEmail(pCustomer.getEmail());
            newCustomer.setNote(pCustomer.getNote());
            newCustomer.setCreateBy(pCustomer.getCreateBy());
            newCustomer.setUpdateBy(pCustomer.getUpdateBy());
            newCustomer.setCreateDate(new Date());

            Customer saveCustomer = customerRepository.save(newCustomer);
            return new ResponseEntity<>(saveCustomer,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/customer/{id}")
    public ResponseEntity<Object> updateCustomer(@Valid @RequestBody Customer pCustomer,@PathVariable("id") int id){
        try {
            Optional<Customer> findCustomer = customerRepository.findById(id);
            if(findCustomer.isPresent()){
                Customer customerData = findCustomer.get();
                customerData.setAddress(pCustomer.getAddress());
                customerData.setContactName(pCustomer.getContactName());
                customerData.setContactTitle(pCustomer.getContactTitle());
                customerData.setMobile(pCustomer.getMobile());
                customerData.setEmail(pCustomer.getEmail());
                customerData.setNote(pCustomer.getNote());
                customerData.setCreateBy(pCustomer.getCreateBy());
                customerData.setUpdateBy(pCustomer.getUpdateBy());
                customerData.setUpdateDate(new Date());

                Customer saveCustomer = customerRepository.save(customerData);
                return new ResponseEntity<>(saveCustomer,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/customer/{id}")
    public ResponseEntity<Customer> deleteCustomerById(@PathVariable("id") int id){
        try {
            Optional<Customer> findCustomer = customerRepository.findById(id);
            if(findCustomer.isPresent()){
                customerRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
