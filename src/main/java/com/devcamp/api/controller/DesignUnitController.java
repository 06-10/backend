package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.DesignUnit;
import com.devcamp.api.repository.DesignUnitRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class DesignUnitController {
    
    @Autowired
    DesignUnitRepository designUnitRepository;

    //GET ALL
    @GetMapping("/designunit")
    public ResponseEntity<List<DesignUnit>> getAllDesignUnit(){
        try {
            List<DesignUnit> allDesignUnit = new ArrayList<>();
            designUnitRepository.findAll().forEach(allDesignUnit::add);
            return new ResponseEntity<>(allDesignUnit,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/designunit/{id}")
    public ResponseEntity<DesignUnit> getDesignUnitById(@PathVariable("id") int id){
        try {
            Optional<DesignUnit> findDesignUnit = designUnitRepository.findById(id);
            DesignUnit designUnitData = findDesignUnit.get();
            return new ResponseEntity<DesignUnit>(designUnitData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/designunit")
    public ResponseEntity<Object> createDesignUnit(@Valid @RequestBody DesignUnit pDesignUnit){
        try {
            DesignUnit newDesignUnit = new DesignUnit();
            newDesignUnit.setName(pDesignUnit.getName());
            newDesignUnit.setDescription(pDesignUnit.getDescription());
            newDesignUnit.setProjects(pDesignUnit.getProjects());
            newDesignUnit.setAddress(pDesignUnit.getAddress());
            newDesignUnit.setPhone(pDesignUnit.getPhone());
            newDesignUnit.setPhone2(pDesignUnit.getPhone2());
            newDesignUnit.setFax(pDesignUnit.getFax());
            newDesignUnit.setEmail(pDesignUnit.getEmail());
            newDesignUnit.setWebsite(pDesignUnit.getWebsite());
            newDesignUnit.setNote(pDesignUnit.getNote());

            DesignUnit saveDesignUnit = designUnitRepository.save(newDesignUnit);
            return new ResponseEntity<>(saveDesignUnit,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/designunit/{id}")
    public ResponseEntity<Object> updateDesignUnit(@Valid @RequestBody DesignUnit pDesignUnit,@PathVariable("id") int id){
        try {
            Optional<DesignUnit> findDesignUnit = designUnitRepository.findById(id);
            if(findDesignUnit.isPresent()){
                DesignUnit designUnitData = findDesignUnit.get();
                designUnitData.setName(pDesignUnit.getName());
                designUnitData.setDescription(pDesignUnit.getDescription());
                designUnitData.setProjects(pDesignUnit.getProjects());
                designUnitData.setAddress(pDesignUnit.getAddress());
                designUnitData.setPhone(pDesignUnit.getPhone());
                designUnitData.setPhone2(pDesignUnit.getPhone2());
                designUnitData.setFax(pDesignUnit.getFax());
                designUnitData.setEmail(pDesignUnit.getEmail());
                designUnitData.setWebsite(pDesignUnit.getWebsite());
                designUnitData.setNote(pDesignUnit.getNote());

                DesignUnit saveDesignUnit = designUnitRepository.save(designUnitData);
                return new ResponseEntity<>(saveDesignUnit,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/designunit/{id}")
    public ResponseEntity<DesignUnit> deleteDesignUnitById(@PathVariable("id") int id){
        try {
            Optional<DesignUnit> findDesignUnit = designUnitRepository.findById(id);
            if(findDesignUnit.isPresent()){
                designUnitRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
