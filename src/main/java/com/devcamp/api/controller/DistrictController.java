package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.District;
import com.devcamp.api.repository.DistrictRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class DistrictController {
    
    @Autowired
    DistrictRepository districtRepository;

    //GET ALL
    @GetMapping("/district")
    public ResponseEntity<List<District>> getAllDistrict(){
        try {
            List<District> allDistrict = new ArrayList<>();
            districtRepository.findAll().forEach(allDistrict::add);
            return new ResponseEntity<>(allDistrict,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/district/{id}")
    public ResponseEntity<District> getDistrictById(@PathVariable("id") int id){
        try {
            Optional<District> findDistrict = districtRepository.findById(id);
            District districtData = findDistrict.get();
            return new ResponseEntity<District>(districtData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/district")
    public ResponseEntity<Object> createDistrict(@Valid @RequestBody District pDistrict){
        try {
            District newDistrict = new District();
            newDistrict.setName(pDistrict.getName());
            newDistrict.setPrefix(pDistrict.getPrefix());
            newDistrict.setProvinceId(pDistrict.getProvinceId());

            District saveDistrict = districtRepository.save(newDistrict);
            return new ResponseEntity<>(saveDistrict,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/district/{id}")
    public ResponseEntity<Object> updateDistrict(@Valid @RequestBody District pDistrict,@PathVariable("id") int id){
        try {
            Optional<District> findDistrict = districtRepository.findById(id);
            if(findDistrict.isPresent()){
                District districtData = findDistrict.get();
                districtData.setName(pDistrict.getName());
                districtData.setPrefix(pDistrict.getPrefix());
                districtData.setProvinceId(pDistrict.getProvinceId());

                District saveDistrict = districtRepository.save(districtData);
                return new ResponseEntity<>(saveDistrict,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/district/{id}")
    public ResponseEntity<District> deleteDistrictById(@PathVariable("id") int id){
        try {
            Optional<District> findDistrict = districtRepository.findById(id);
            if(findDistrict.isPresent()){
                districtRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
