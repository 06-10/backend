package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Employee;
import com.devcamp.api.repository.EmployeeRepository;


@RestController
@CrossOrigin
@RequestMapping("/")
public class EmployeeController {
    
    @Autowired
    EmployeeRepository employeeRepository;

    //GET ALL
    @GetMapping("/employee")
    public ResponseEntity<List<Employee>> getAllEmployee(){
        try {
            List<Employee> allEmployees = new ArrayList<>();
            employeeRepository.findAll().forEach(allEmployees::add);
            return new ResponseEntity<>(allEmployees,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") int id){
        try {
            Optional<Employee> findEmployee = employeeRepository.findById(id);
            Employee employeeData = findEmployee.get();
            return new ResponseEntity<Employee>(employeeData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/employee")
    public ResponseEntity<Object> createEmployee(@RequestBody Employee pEmployee){
        try {
            Employee newEmployee = new Employee();
            newEmployee.setLastName(pEmployee.getLastName());
            newEmployee.setFirstName(pEmployee.getFirstName());
            newEmployee.setTitle(pEmployee.getTitle());
            newEmployee.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
            newEmployee.setBirthDate(pEmployee.getBirthDate());
            newEmployee.setHireDate(pEmployee.getHireDate());
            newEmployee.setAddress(pEmployee.getAddress());
            newEmployee.setCity(pEmployee.getCity());
            newEmployee.setRegion(pEmployee.getRegion());
            newEmployee.setPostalCode(pEmployee.getPostalCode());
            newEmployee.setCountry(pEmployee.getCountry());
            newEmployee.setHomePhone(pEmployee.getHomePhone());
            newEmployee.setExtension(pEmployee.getExtension());
            newEmployee.setPhoto(pEmployee.getPhoto());
            newEmployee.setNotes(pEmployee.getNotes());
            newEmployee.setReportsTo(pEmployee.getReportsTo());
            newEmployee.setUsername(pEmployee.getUsername());
            newEmployee.setPassword(pEmployee.getPassword());
            newEmployee.setEmail(pEmployee.getEmail());
            newEmployee.setActivated(pEmployee.getActivated());
            newEmployee.setProfile(pEmployee.getProfile());
            newEmployee.setUserLevel(pEmployee.getUserLevel());

            Employee saveEmployee = employeeRepository.save(newEmployee);
            return new ResponseEntity<>(saveEmployee,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/employee/{id}")
    public ResponseEntity<Object> updateEmployee(@Valid @RequestBody Employee pEmployee,@PathVariable("id") int id){
        try {
            Optional<Employee> findEmployee = employeeRepository.findById(id);
            if(findEmployee.isPresent()){
                Employee employeeData = findEmployee.get();
                employeeData.setLastName(pEmployee.getLastName());
                employeeData.setFirstName(pEmployee.getFirstName());
                employeeData.setTitle(pEmployee.getTitle());
                employeeData.setTitleOfCourtesy(pEmployee.getTitleOfCourtesy());
                employeeData.setBirthDate(pEmployee.getBirthDate());
                employeeData.setHireDate(pEmployee.getHireDate());
                employeeData.setAddress(pEmployee.getAddress());
                employeeData.setCity(pEmployee.getCity());
                employeeData.setRegion(pEmployee.getRegion());
                employeeData.setPostalCode(pEmployee.getPostalCode());
                employeeData.setCountry(pEmployee.getCountry());
                employeeData.setHomePhone(pEmployee.getHomePhone());
                employeeData.setExtension(pEmployee.getExtension());
                employeeData.setPhoto(pEmployee.getPhoto());
                employeeData.setNotes(pEmployee.getNotes());
                employeeData.setReportsTo(pEmployee.getReportsTo());
                employeeData.setUsername(pEmployee.getUsername());
                employeeData.setPassword(pEmployee.getPassword());
                employeeData.setEmail(pEmployee.getEmail());
                employeeData.setActivated(pEmployee.getActivated());
                employeeData.setProfile(pEmployee.getProfile());
                employeeData.setUserLevel(pEmployee.getUserLevel());

                Employee saveEmployee = employeeRepository.save(employeeData);
                return new ResponseEntity<>(saveEmployee,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Employee> deleteContractorById(@PathVariable("id") int id){
        try {
            Optional<Employee> findEmployee = employeeRepository.findById(id);
            if(findEmployee.isPresent()){
                employeeRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
