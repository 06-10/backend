package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Investor;
import com.devcamp.api.repository.InvestorRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class InvestorController {
    
    @Autowired
    InvestorRepository investorRepository;

    //GET ALL
    @GetMapping("/investor")
    public ResponseEntity<List<Investor>> getAllInvestor(){
        try {
            List<Investor> allInvestor = new ArrayList<>();
            investorRepository.findAll().forEach(allInvestor::add);
            return new ResponseEntity<>(allInvestor,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/investor/{id}")
    public ResponseEntity<Investor> getInvestorById(@PathVariable("id") int id){
        try {
            Optional<Investor> findInvestor = investorRepository.findById(id);
            Investor investorData = findInvestor.get();
            return new ResponseEntity<Investor>(investorData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/investor")
    public ResponseEntity<Object> createInvestor(@Valid @RequestBody Investor pInvestor){
        try {
            Investor newInvestor = new Investor();
            newInvestor.setName(pInvestor.getName());
            newInvestor.setDescription(pInvestor.getDescription());
            newInvestor.setProjects(pInvestor.getProjects());
            newInvestor.setAddress(pInvestor.getAddress());
            newInvestor.setPhone(pInvestor.getPhone());
            newInvestor.setPhone2(pInvestor.getPhone2());
            newInvestor.setFax(pInvestor.getFax());
            newInvestor.setEmail(pInvestor.getEmail());
            newInvestor.setWebsite(pInvestor.getWebsite());
            newInvestor.setNote(pInvestor.getNote());

            Investor saveInvestor = investorRepository.save(newInvestor);
            return new ResponseEntity<>(saveInvestor,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/investor/{id}")
    public ResponseEntity<Object> updateInvestor(@Valid @RequestBody Investor pInvestor,@PathVariable("id") int id){
        try {
            Optional<Investor> findInvestor = investorRepository.findById(id);
            if(findInvestor.isPresent()){
                Investor investorData = findInvestor.get();
                investorData.setName(pInvestor.getName());
                investorData.setDescription(pInvestor.getDescription());
                investorData.setProjects(pInvestor.getProjects());
                investorData.setAddress(pInvestor.getAddress());
                investorData.setPhone(pInvestor.getPhone());
                investorData.setPhone2(pInvestor.getPhone2());
                investorData.setFax(pInvestor.getFax());
                investorData.setEmail(pInvestor.getEmail());
                investorData.setWebsite(pInvestor.getWebsite());
                investorData.setNote(pInvestor.getNote());

                Investor saveInvestor = investorRepository.save(investorData);
                return new ResponseEntity<>(saveInvestor,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/investor/{id}")
    public ResponseEntity<Investor> deleteInvestorById(@PathVariable("id") int id){
        try {
            Optional<Investor> findInvestor = investorRepository.findById(id);
            if(findInvestor.isPresent()){
                investorRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
