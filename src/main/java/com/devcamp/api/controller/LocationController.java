package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Location;
import com.devcamp.api.repository.LocationRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class LocationController {
    
    @Autowired
    LocationRepository locationRepository;

    //GET ALL
    @GetMapping("/location")
    public ResponseEntity<List<Location>> getAllLocation(){
        try {
            List<Location> allLocation = new ArrayList<>();
            locationRepository.findAll().forEach(allLocation::add);
            return new ResponseEntity<>(allLocation,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/location/{id}")
    public ResponseEntity<Location> getLocationById(@PathVariable("id") int id){
        try {
            Optional<Location> findLocation = locationRepository.findById(id);
            Location locationData = findLocation.get();
            return new ResponseEntity<Location>(locationData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/location")
    public ResponseEntity<Object> createLocation(@Valid @RequestBody Location pLocation){
        try {
            Location newLocation = new Location();
            newLocation.setLatitude(pLocation.getLatitude());
            newLocation.setLongitude(pLocation.getLongitude());

            Location saveLocation = locationRepository.save(newLocation);
            return new ResponseEntity<>(saveLocation,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/location/{id}")
    public ResponseEntity<Object> updateLocation(@Valid @RequestBody Location pLocation,@PathVariable("id") int id){
        try {
            Optional<Location> findLocation = locationRepository.findById(id);
            if(findLocation.isPresent()){
                Location locationData = findLocation.get();
                locationData.setLatitude(pLocation.getLatitude());
                locationData.setLongitude(pLocation.getLongitude());

                Location saveLocation = locationRepository.save(locationData);
                return new ResponseEntity<>(saveLocation,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/location/{id}")
    public ResponseEntity<Location> deleteLocationById(@PathVariable("id") int id){
        try {
            Optional<Location> findLocation = locationRepository.findById(id);
            if(findLocation.isPresent()){
                locationRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
