package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Date;

import com.devcamp.api.model.MasterLayout;
import com.devcamp.api.repository.MasterLayoutRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class MasterLayoutController {
    
    @Autowired
    MasterLayoutRepository masterLayoutRepository;

    //GET ALL
    @GetMapping("/masterlayout")
    public ResponseEntity<List<MasterLayout>> getAllMasterLayout(){
        try {
            List<MasterLayout> allMasterLayout = new ArrayList<>();
            masterLayoutRepository.findAll().forEach(allMasterLayout::add);
            return new ResponseEntity<>(allMasterLayout,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/masterlayout/{id}")
    public ResponseEntity<MasterLayout> getMasterLayoutById(@PathVariable("id") int id){
        try {
            Optional<MasterLayout> findMasterLayout = masterLayoutRepository.findById(id);
            MasterLayout masterLayoutData = findMasterLayout.get();
            return new ResponseEntity<MasterLayout>(masterLayoutData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/masterlayout")
    public ResponseEntity<Object> createMasterLayout(@Valid @RequestBody MasterLayout pMasterLayout){
        try {
            MasterLayout newMasterLayout = new MasterLayout();
            newMasterLayout.setName(pMasterLayout.getName());
            newMasterLayout.setDescription(pMasterLayout.getDescription());
            newMasterLayout.setProjectId(pMasterLayout.getProjectId());
            newMasterLayout.setAcreage(pMasterLayout.getAcreage());
            newMasterLayout.setApartmentList(pMasterLayout.getApartmentList());
            newMasterLayout.setPhoto(pMasterLayout.getPhoto());
            newMasterLayout.setDateCreate(new Date());

            MasterLayout saveMasterLayout = masterLayoutRepository.save(newMasterLayout);
            return new ResponseEntity<>(saveMasterLayout,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/masterlayout/{id}")
    public ResponseEntity<Object> updateMasterLayout(@Valid @RequestBody MasterLayout pMasterLayout,@PathVariable("id") int id){
        try {
            Optional<MasterLayout> findMasterLayout = masterLayoutRepository.findById(id);
            if(findMasterLayout.isPresent()){
                MasterLayout masterLayoutData = findMasterLayout.get();
                masterLayoutData.setName(pMasterLayout.getName());
                masterLayoutData.setDescription(pMasterLayout.getDescription());
                masterLayoutData.setProjectId(pMasterLayout.getProjectId());
                masterLayoutData.setAcreage(pMasterLayout.getAcreage());
                masterLayoutData.setApartmentList(pMasterLayout.getApartmentList());
                masterLayoutData.setPhoto(pMasterLayout.getPhoto());
                masterLayoutData.setDateUpdate(new Date());

                MasterLayout saveMasterLayout = masterLayoutRepository.save(masterLayoutData);
                return new ResponseEntity<>(saveMasterLayout,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/masterlayout/{id}")
    public ResponseEntity<MasterLayout> deleteMasterLayoutById(@PathVariable("id") int id){
        try {
            Optional<MasterLayout> findMasterLayout = masterLayoutRepository.findById(id);
            if(findMasterLayout.isPresent()){
                masterLayoutRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
