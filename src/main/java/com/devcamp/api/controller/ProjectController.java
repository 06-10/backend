package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Project;
import com.devcamp.api.repository.ProjectRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class ProjectController {
    
    @Autowired
    ProjectRepository projectRepository;
    
    //GET BY PROVINCE ID
    
    @GetMapping("/project/provinceId/{id}")
    public ResponseEntity<List<Project>> getByProvinceId(@PathVariable("id")  int id ) {
        try {
            List<Project> projects = projectRepository.findByProvinceId(id);
            return new ResponseEntity<>(projects,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
   }

    //GET ALL
    @GetMapping("/project")
    public ResponseEntity<List<Project>> getAllProject(){
        try {
            List<Project> allProject = new ArrayList<>();
            projectRepository.findAll().forEach(allProject::add);
            return new ResponseEntity<>(allProject,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/project/{id}")
    public ResponseEntity<Project> getProjectById(@PathVariable("id") int id){
        try {
            Optional<Project> findProject = projectRepository.findById(id);
            Project projectData = findProject.get();
            return new ResponseEntity<Project>(projectData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/project")
    public ResponseEntity<Object> createProject(@Valid @RequestBody Project pProject){
        try {
            Project newProject = new Project();
            newProject.setName(pProject.getName());
            newProject.setProvinceId(pProject.getProvinceId());
            newProject.setDistrictId(pProject.getDistrictId());
            newProject.setWardId(pProject.getWardId());
            newProject.setStreetId(pProject.getStreetId());
            newProject.setAddress(pProject.getAddress());
            newProject.setSlogan(pProject.getSlogan());
            newProject.setDescription(pProject.getDescription());
            newProject.setAcreage(pProject.getAcreage());
            newProject.setConstructArea(pProject.getConstructArea());
            newProject.setNumBlock(pProject.getNumBlock());
            newProject.setNumFloors(pProject.getNumFloors());
            newProject.setNumApartment(pProject.getNumApartment());
            newProject.setInvestor(pProject.getInvestor());
            newProject.setConstructionContractor(pProject.getConstructionContractor());
            newProject.setDesignUnit(pProject.getDesignUnit());
            newProject.setUtilities(pProject.getUtilities());
            newProject.setRegionLink(pProject.getRegionLink());
            newProject.setPhoto(pProject.getPhoto());
            newProject.setLat(pProject.getLat());
            newProject.setLng(pProject.getLng());
            newProject.setAccepted(false);

            Project saveProject = projectRepository.save(newProject);
            return new ResponseEntity<>(saveProject,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/project/{id}")
    public ResponseEntity<Object> updateProject(@Valid @RequestBody Project pProject,@PathVariable("id") int id){
        try {
            Optional<Project> findProject = projectRepository.findById(id);
            if(findProject.isPresent()){
                Project projectData = findProject.get();
                projectData.setName(pProject.getName());
                projectData.setProvinceId(pProject.getProvinceId());
                projectData.setDistrictId(pProject.getDistrictId());
                projectData.setWardId(pProject.getWardId());
                projectData.setStreetId(pProject.getStreetId());
                projectData.setAddress(pProject.getAddress());
                projectData.setSlogan(pProject.getSlogan());
                projectData.setDescription(pProject.getDescription());
                projectData.setAcreage(pProject.getAcreage());
                projectData.setConstructArea(pProject.getConstructArea());
                projectData.setNumBlock(pProject.getNumBlock());
                projectData.setNumFloors(pProject.getNumFloors());
                projectData.setNumApartment(pProject.getNumApartment());
                projectData.setInvestor(pProject.getInvestor());
                projectData.setConstructionContractor(pProject.getConstructionContractor());
                projectData.setDesignUnit(pProject.getDesignUnit());
                projectData.setUtilities(pProject.getUtilities());
                projectData.setRegionLink(pProject.getRegionLink());
                projectData.setPhoto(pProject.getPhoto());
                projectData.setLat(pProject.getLat());
                projectData.setLng(pProject.getLng());

                Project saveProject = projectRepository.save(projectData);
                return new ResponseEntity<>(saveProject,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/project/{id}")
    public ResponseEntity<Project> deleteProjectById(@PathVariable("id") int id){
        try {
            Optional<Project> findProject = projectRepository.findById(id);
            if(findProject.isPresent()){
                projectRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    

    @GetMapping("/project/{id}/accepted")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<Object> acceptProject(@PathVariable("id") int id){
        try {
            Optional<Project> findProject = projectRepository.findById(id);
            if(findProject.isPresent()){
                Project projectData = findProject.get();
                projectData.setAccepted(true);

                Project saveProject = projectRepository.save(projectData);
                return new ResponseEntity<>(saveProject,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/project/{id}/denined")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<Object> deniedProject(@PathVariable("id") int id){
        try {
            Optional<Project> findProject = projectRepository.findById(id);
            if(findProject.isPresent()){
                Project projectData = findProject.get();
                projectData.setAccepted(false);

                Project saveProject = projectRepository.save(projectData);
                return new ResponseEntity<>(saveProject,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
