package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Province;
import com.devcamp.api.repository.ProvinceRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class ProvinceController {
    
    @Autowired
    ProvinceRepository provinceRepository;

    //GET ALL
    @GetMapping("/province")
    public ResponseEntity<List<Province>> getAllProvince(){
        try {
            List<Province> allProvince = new ArrayList<>();
            provinceRepository.findAll().forEach(allProvince::add);
            return new ResponseEntity<>(allProvince,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/province/{id}")
    public ResponseEntity<Province> getProvinceById(@PathVariable("id") int id){
        try {
            Optional<Province> findProvince = provinceRepository.findById(id);
            Province provinceData = findProvince.get();
            return new ResponseEntity<Province>(provinceData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/province")
    public ResponseEntity<Object> createProvince(@Valid @RequestBody Province pProvince){
        try {
            Province newProvince = new Province();
            newProvince.setCode(pProvince.getCode());
            newProvince.setName(pProvince.getName());

            Province saveProvince = provinceRepository.save(newProvince);
            return new ResponseEntity<>(saveProvince,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/province/{id}")
    public ResponseEntity<Object> updateProvince(@Valid @RequestBody Province pProvince,@PathVariable("id") int id){
        try {
            Optional<Province> findProvince = provinceRepository.findById(id);
            if(findProvince.isPresent()){
                Province provinceData = findProvince.get();
                provinceData.setCode(pProvince.getCode());
                provinceData.setName(pProvince.getName());

                Province saveProvince = provinceRepository.save(provinceData);
                return new ResponseEntity<>(saveProvince,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/province/{id}")
    public ResponseEntity<Province> deleteProvinceById(@PathVariable("id") int id){
        try {
            Optional<Province> findProvince = provinceRepository.findById(id);
            if(findProvince.isPresent()){
                provinceRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
