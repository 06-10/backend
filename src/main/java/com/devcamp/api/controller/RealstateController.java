package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Realstate;
import com.devcamp.api.repository.RealStateRepository;

@CrossOrigin
@RestController
public class RealstateController {
    
    @Autowired
    RealStateRepository realStateRepository;
    

    @GetMapping("/realstate")
    public ResponseEntity<List<Realstate>> getAllRealstate(){
        try {
            List<Realstate> allRealstate = new ArrayList<>();
            realStateRepository.findAll().forEach(allRealstate::add);
            return new ResponseEntity<>(allRealstate,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/realstate/{id}")
    public ResponseEntity<?> deleteRealstateById(@PathVariable("id") int id){
        try {
            realStateRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/realstate")
    public ResponseEntity<Object> createRealstate(@Valid @RequestBody Realstate pRealstate){
        try {
            Realstate newRealstate = new Realstate();
            newRealstate.setType(pRealstate.getType());
            newRealstate.setRequest(pRealstate.getRequest());
            newRealstate.setProvince_id(pRealstate.getProvince_id());
            newRealstate.setDistrict_id(pRealstate.getDistrict_id());
            newRealstate.setWards_id(pRealstate.getWards_id());
            newRealstate.setStreet_id(pRealstate.getStreet_id());
            newRealstate.setProject_id(pRealstate.getProject_id());
            newRealstate.setAddress(pRealstate.getAddress());
            newRealstate.setCustomer_id(pRealstate.getCustomer_id());
            newRealstate.setPrice(pRealstate.getPrice());
            newRealstate.setPrice_min(pRealstate.getPrice_min());
            newRealstate.setPrice_time(pRealstate.getPrice_time());
            newRealstate.setDate_create(pRealstate.getDate_create());
            newRealstate.setAcreage(pRealstate.getAcreage());
            newRealstate.setDirection(pRealstate.getDirection());
            newRealstate.setTotal_floors(pRealstate.getTotal_floors());
            newRealstate.setNumber_floors(pRealstate.getNumber_floors());
            newRealstate.setBath(pRealstate.getBath());
            newRealstate.setApart_code(pRealstate.getApart_code());
            newRealstate.setWall_area(pRealstate.getWall_area());
            newRealstate.setBedroom(pRealstate.getBedroom());
            newRealstate.setBalcony(pRealstate.getBalcony());
            newRealstate.setLandscape_view(pRealstate.getLandscape_view());
            newRealstate.setApart_loca(pRealstate.getApart_loca());
            newRealstate.setApart_type(pRealstate.getApart_type());
            newRealstate.setFurniture_type(pRealstate.getFurniture_type());
            newRealstate.setPrice_rent(pRealstate.getPrice_rent());
            newRealstate.setReturn_rate(pRealstate.getReturn_rate());
            newRealstate.setLegal_doc(pRealstate.getLegal_doc());
            newRealstate.setDescription(pRealstate.getDescription());
            newRealstate.setWidth_y(pRealstate.getWidth_y());
            newRealstate.setLong_x(pRealstate.getLong_x());
            newRealstate.setStreet_house(pRealstate.getStreet_house());
            newRealstate.setFSBO(pRealstate.getFSBO());
            newRealstate.setView_num(pRealstate.getView_num());
            newRealstate.setCreate_by(pRealstate.getCreate_by());
            newRealstate.setUpdate_by(pRealstate.getUpdate_by());
            newRealstate.setShape(pRealstate.getShape());
            newRealstate.setDistance2facade(pRealstate.getDistance2facade());
            newRealstate.setAdjacent_alley_min_width(pRealstate.getAdjacent_alley_min_width());
            newRealstate.setAdjacent_facade_num(pRealstate.getAdjacent_facade_num());
            newRealstate.setAdjacent_road(pRealstate.getAdjacent_road());
            newRealstate.setAlley_min_width(pRealstate.getAlley_min_width());
            newRealstate.setFactor(pRealstate.getFactor());
            newRealstate.setStructure(pRealstate.getStructure());
            newRealstate.setDTSXD(pRealstate.getDTSXD());
            newRealstate.setCLCL(pRealstate.getCLCL());
            newRealstate.setCTXD_price(pRealstate.getCTXD_price());
            newRealstate.setCTXD_value(pRealstate.getCTXD_value());
            newRealstate.setPhoto(pRealstate.getPhoto());
            newRealstate.set_lat(pRealstate.get_lat());
            newRealstate.set_lng(pRealstate.get_lng());
            Realstate saveRealstate = realStateRepository.save(newRealstate);
            return new ResponseEntity<>(saveRealstate,HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/realstate/{id}")
    public ResponseEntity<Object> updateRealstate(@PathVariable("id") int id,@Valid @RequestBody Realstate pRealstate){
        try {
            Optional<Realstate> findRealstate = realStateRepository.findById(id);
            if(findRealstate.isPresent()){
                Realstate realstateData = findRealstate.get();
                realstateData.setType(pRealstate.getType());
                realstateData.setRequest(pRealstate.getRequest());
                realstateData.setProvince_id(pRealstate.getProvince_id());
                realstateData.setDistrict_id(pRealstate.getDistrict_id());
                realstateData.setWards_id(pRealstate.getWards_id());
                realstateData.setStreet_id(pRealstate.getStreet_id());
                realstateData.setProject_id(pRealstate.getProject_id());
                realstateData.setAddress(pRealstate.getAddress());
                realstateData.setCustomer_id(pRealstate.getCustomer_id());
                realstateData.setPrice(pRealstate.getPrice());
                realstateData.setPrice_min(pRealstate.getPrice_min());
                realstateData.setPrice_time(pRealstate.getPrice_time());
                realstateData.setDate_create(pRealstate.getDate_create());
                realstateData.setAcreage(pRealstate.getAcreage());
                realstateData.setDirection(pRealstate.getDirection());
                realstateData.setTotal_floors(pRealstate.getTotal_floors());
                realstateData.setNumber_floors(pRealstate.getNumber_floors());
                realstateData.setBath(pRealstate.getBath());
                realstateData.setApart_code(pRealstate.getApart_code());
                realstateData.setWall_area(pRealstate.getWall_area());
                realstateData.setBedroom(pRealstate.getBedroom());
                realstateData.setBalcony(pRealstate.getBalcony());
                realstateData.setLandscape_view(pRealstate.getLandscape_view());
                realstateData.setApart_loca(pRealstate.getApart_loca());
                realstateData.setApart_type(pRealstate.getApart_type());
                realstateData.setFurniture_type(pRealstate.getFurniture_type());
                realstateData.setPrice_rent(pRealstate.getPrice_rent());
                realstateData.setReturn_rate(pRealstate.getReturn_rate());
                realstateData.setLegal_doc(pRealstate.getLegal_doc());
                realstateData.setDescription(pRealstate.getDescription());
                realstateData.setWidth_y(pRealstate.getWidth_y());
                realstateData.setLong_x(pRealstate.getLong_x());
                realstateData.setStreet_house(pRealstate.getStreet_house());
                realstateData.setFSBO(pRealstate.getFSBO());
                realstateData.setView_num(pRealstate.getView_num());
                realstateData.setCreate_by(pRealstate.getCreate_by());
                realstateData.setUpdate_by(pRealstate.getUpdate_by());
                realstateData.setShape(pRealstate.getShape());
                realstateData.setDistance2facade(pRealstate.getDistance2facade());
                realstateData.setAdjacent_alley_min_width(pRealstate.getAdjacent_alley_min_width());
                realstateData.setAdjacent_facade_num(pRealstate.getAdjacent_facade_num());
                realstateData.setAdjacent_road(pRealstate.getAdjacent_road());
                realstateData.setAlley_min_width(pRealstate.getAlley_min_width());
                realstateData.setFactor(pRealstate.getFactor());
                realstateData.setStructure(pRealstate.getStructure());
                realstateData.setDTSXD(pRealstate.getDTSXD());
                realstateData.setCLCL(pRealstate.getCLCL());
                realstateData.setCTXD_price(pRealstate.getCTXD_price());
                realstateData.setCTXD_value(pRealstate.getCTXD_value());
                realstateData.setPhoto(pRealstate.getPhoto());
                realstateData.set_lat(pRealstate.get_lat());
                realstateData.set_lng(pRealstate.get_lng());
                Realstate saveRealstate = realStateRepository.save(realstateData);
                return new ResponseEntity<>(saveRealstate,HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
