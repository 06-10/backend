package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.RegionLink;
import com.devcamp.api.repository.RegionLinkRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class RegionLinkController {
    
    @Autowired
    RegionLinkRepository regionLinkRepository;

    //GET ALL
    @GetMapping("/regionlink")
    public ResponseEntity<List<RegionLink>> getAllRegionLink(){
        try {
            List<RegionLink> allRegionLink = new ArrayList<>();
            regionLinkRepository.findAll().forEach(allRegionLink::add);
            return new ResponseEntity<>(allRegionLink,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/regionlink/{id}")
    public ResponseEntity<RegionLink> getRegionLinkById(@PathVariable("id") int id){
        try {
            Optional<RegionLink> findRegionLink = regionLinkRepository.findById(id);
            RegionLink regionLinkData = findRegionLink.get();
            return new ResponseEntity<RegionLink>(regionLinkData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/regionlink")
    public ResponseEntity<Object> createRegionLink(@Valid @RequestBody RegionLink pRegionLink){
        try {
            RegionLink newRegionLink = new RegionLink();
            newRegionLink.setAddress(pRegionLink.getAddress());
            newRegionLink.setDescription(pRegionLink.getDescription());
            newRegionLink.setLat(pRegionLink.getLat());
            newRegionLink.setLng(pRegionLink.getLng());
            newRegionLink.setName(pRegionLink.getName());
            newRegionLink.setPhoto(pRegionLink.getPhoto());

            RegionLink saveRegionLink = regionLinkRepository.save(newRegionLink);
            return new ResponseEntity<>(saveRegionLink,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/regionlink/{id}")
    public ResponseEntity<Object> updateRegionLink(@Valid @RequestBody RegionLink pRegionLink,@PathVariable("id") int id){
        try {
            Optional<RegionLink> findRegionLink = regionLinkRepository.findById(id);
            if(findRegionLink.isPresent()){
                RegionLink regionLinkData = findRegionLink.get();
                regionLinkData.setAddress(pRegionLink.getAddress());
                regionLinkData.setDescription(pRegionLink.getDescription());
                regionLinkData.setLat(pRegionLink.getLat());
                regionLinkData.setLng(pRegionLink.getLng());
                regionLinkData.setName(pRegionLink.getName());
                regionLinkData.setPhoto(pRegionLink.getPhoto());

                RegionLink saveRegionLink = regionLinkRepository.save(regionLinkData);
                return new ResponseEntity<>(saveRegionLink,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/regionlink/{id}")
    public ResponseEntity<RegionLink> deleteRegionLinkById(@PathVariable("id") int id){
        try {
            Optional<RegionLink> findRegionLink = regionLinkRepository.findById(id);
            if(findRegionLink.isPresent()){
                regionLinkRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
