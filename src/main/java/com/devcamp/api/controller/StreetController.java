package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Street;
import com.devcamp.api.repository.StreetRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class StreetController {
    
    @Autowired
    StreetRepository streetRepository;

    //GET ALL
    @GetMapping("/street")
    public ResponseEntity<List<Street>> getAllStreet(){
        try {
            List<Street> allStreet = new ArrayList<>();
            streetRepository.findAll().forEach(allStreet::add);
            return new ResponseEntity<>(allStreet,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/street/{id}")
    public ResponseEntity<Street> getStreetById(@PathVariable("id") int id){
        try {
            Optional<Street> findStreet = streetRepository.findById(id);
            Street streetData = findStreet.get();
            return new ResponseEntity<Street>(streetData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/street")
    public ResponseEntity<Object> createStreet(@Valid @RequestBody Street pStreet){
        try {
            Street newStreet = new Street();
            newStreet.setDistrictId(pStreet.getDistrictId());
            newStreet.setName(pStreet.getName());
            newStreet.setPrefix(pStreet.getPrefix());
            newStreet.setProvinceId(pStreet.getProvinceId());

            Street saveStreet = streetRepository.save(newStreet);
            return new ResponseEntity<>(saveStreet,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/street/{id}")
    public ResponseEntity<Object> updateStreet(@Valid @RequestBody Street pStreet,@PathVariable("id") int id){
        try {
            Optional<Street> findStreet = streetRepository.findById(id);
            if(findStreet.isPresent()){
                Street streetData = findStreet.get();
                streetData.setDistrictId(pStreet.getDistrictId());
                streetData.setName(pStreet.getName());
                streetData.setPrefix(pStreet.getPrefix());
                streetData.setProvinceId(pStreet.getProvinceId());

                Street saveStreet = streetRepository.save(streetData);
                return new ResponseEntity<>(saveStreet,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/Street/{id}")
    public ResponseEntity<Street> deleteStreetById(@PathVariable("id") int id){
        try {
            Optional<Street> findStreet = streetRepository.findById(id);
            if(findStreet.isPresent()){
                streetRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
