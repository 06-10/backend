package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Subscriptions;
import com.devcamp.api.repository.SubscriptionsRepository;



@RestController
@CrossOrigin
@RequestMapping("/")
public class SubscriptionsController {
    
    @Autowired
    SubscriptionsRepository subscriptionsRepository;

    //GET ALL
    @GetMapping("/subscriptions")
    public ResponseEntity<List<Subscriptions>> getAllsubscriptions(){
        try {
            List<Subscriptions> allsubscriptions = new ArrayList<>();
            subscriptionsRepository.findAll().forEach(allsubscriptions::add);
            return new ResponseEntity<>(allsubscriptions,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/subscriptions/{id}")
    public ResponseEntity<Subscriptions> getSubscriptionsById(@PathVariable("id") int id){
        try {
            Optional<Subscriptions> findSubscriptions = subscriptionsRepository.findById(id);
            Subscriptions subscriptionsData = findSubscriptions.get();
            return new ResponseEntity<Subscriptions>(subscriptionsData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/subscriptions")
    public ResponseEntity<Object> createSubscriptions(@Valid @RequestBody Subscriptions pSubscriptions){
        try {
            Subscriptions newSubscriptions = new Subscriptions();
            newSubscriptions.setUser(pSubscriptions.getUser());
            newSubscriptions.setEndpoint(pSubscriptions.getEndpoint());
            newSubscriptions.setPublickey(pSubscriptions.getPublickey());
            newSubscriptions.setAuthenticationtoken(pSubscriptions.getAuthenticationtoken());
            newSubscriptions.setContentencoding(pSubscriptions.getContentencoding());

            Subscriptions saveSubscriptions = subscriptionsRepository.save(newSubscriptions);
            return new ResponseEntity<>(saveSubscriptions,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/subscriptions/{id}")
    public ResponseEntity<Object> updateSubscriptions(@Valid @RequestBody Subscriptions pSubscriptions,@PathVariable("id") int id){
        try {
            Optional<Subscriptions> findSubscriptions = subscriptionsRepository.findById(id);
            if(findSubscriptions.isPresent()){
                Subscriptions subscriptionsData = findSubscriptions.get();
                subscriptionsData.setUser(pSubscriptions.getUser());
                subscriptionsData.setEndpoint(pSubscriptions.getEndpoint());
                subscriptionsData.setPublickey(pSubscriptions.getPublickey());
                subscriptionsData.setAuthenticationtoken(pSubscriptions.getAuthenticationtoken());
                subscriptionsData.setContentencoding(pSubscriptions.getContentencoding());

                Subscriptions saveSubscriptions = subscriptionsRepository.save(subscriptionsData);
                return new ResponseEntity<>(saveSubscriptions,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/subscriptions/{id}")
    public ResponseEntity<Subscriptions> deleteSubscriptionsById(@PathVariable("id") int id){
        try {
            Optional<Subscriptions> findSubscriptions = subscriptionsRepository.findById(id);
            if(findSubscriptions.isPresent()){
                subscriptionsRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
