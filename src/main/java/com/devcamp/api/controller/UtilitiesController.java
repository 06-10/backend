package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Utilities;
import com.devcamp.api.repository.UtilitiesRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class UtilitiesController {
    
    @Autowired
    UtilitiesRepository utilitiesRepository;

    //GET ALL
    @GetMapping("/utilities")
    public ResponseEntity<List<Utilities>> getAllUtilities(){
        try {
            List<Utilities> allUtilities = new ArrayList<>();
            utilitiesRepository.findAll().forEach(allUtilities::add);
            return new ResponseEntity<>(allUtilities,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/utilities/{id}")
    public ResponseEntity<Utilities> getUtilitiesById(@PathVariable("id") int id){
        try {
            Optional<Utilities> findUtilities = utilitiesRepository.findById(id);
            Utilities utilitiesData = findUtilities.get();
            return new ResponseEntity<Utilities>(utilitiesData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE 
    @PostMapping("/utilities")
    public ResponseEntity<Object> createUtilities(@Valid @RequestBody Utilities pUtilities){
        try {
            Utilities newUtilities = new Utilities();
            newUtilities.setName(pUtilities.getName());
            newUtilities.setDescription(pUtilities.getDescription());
            newUtilities.setPhoto(pUtilities.getPhoto());

            Utilities saveUtilities = utilitiesRepository.save(newUtilities);
            return new ResponseEntity<>(saveUtilities,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/utilities/{id}")
    public ResponseEntity<Object> updateUtilities(@Valid @RequestBody Utilities pUtilities,@PathVariable("id") int id){
        try {
            Optional<Utilities> findUtilities = utilitiesRepository.findById(id);
            if(findUtilities.isPresent()){
                Utilities utilitiesData = findUtilities.get();
                utilitiesData.setName(pUtilities.getName());
                utilitiesData.setDescription(pUtilities.getDescription());
                utilitiesData.setPhoto(pUtilities.getPhoto());


                Utilities saveUtilities = utilitiesRepository.save(utilitiesData);
                return new ResponseEntity<>(saveUtilities,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/utilities/{id}")
    public ResponseEntity<Utilities> deleteUtilitiesById(@PathVariable("id") int id){
        try {
            Optional<Utilities> findUtilities = utilitiesRepository.findById(id);
            if(findUtilities.isPresent()){
                utilitiesRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
