package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import com.devcamp.api.model.Ward;
import com.devcamp.api.repository.WardRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class WardController {
    
    @Autowired
    WardRepository wardRepository;

    //GET ALL
    @GetMapping("/ward")
    public ResponseEntity<List<Ward>> getAllWard(){
        try {
            List<Ward> allWard = new ArrayList<>();
            wardRepository.findAll().forEach(allWard::add);
            return new ResponseEntity<>(allWard,HttpStatus.OK);
        }   
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //GET BY ID
    @GetMapping("/ward/{id}")
    public ResponseEntity<Ward> getWardById(@PathVariable("id") int id){
        try {
            Optional<Ward> findWard = wardRepository.findById(id);
            Ward wardData = findWard.get();
            return new ResponseEntity<Ward>(wardData,HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //CREATE
    @PostMapping("/ward")
    public ResponseEntity<Object> createWard(@Valid @RequestBody Ward pWard){
        try {
            Ward newWard = new Ward();
            newWard.setName(pWard.getName());
            newWard.setPrefix(pWard.getPrefix());
            newWard.setProvinceId(pWard.getProvinceId());
            newWard.setDistrictId(pWard.getDistrictId());

            Ward saveWard = wardRepository.save(newWard);
            return new ResponseEntity<>(saveWard,HttpStatus.CREATED);
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //UPDATE 
    @PutMapping("/ward/{id}")
    public ResponseEntity<Object> updateWard(@Valid @RequestBody Ward pWard,@PathVariable("id") int id){
        try {
            Optional<Ward> findWard = wardRepository.findById(id);
            if(findWard.isPresent()){
                Ward wardData = findWard.get();
                wardData.setName(pWard.getName());
                wardData.setPrefix(pWard.getPrefix());
                wardData.setProvinceId(pWard.getProvinceId());
                wardData.setDistrictId(pWard.getDistrictId());

                Ward saveWard = wardRepository.save(wardData);
                return new ResponseEntity<>(saveWard,HttpStatus.CREATED); 
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //DELETE BY ID
    @DeleteMapping("/ward/{id}")
    public ResponseEntity<Ward> deleteWardById(@PathVariable("id") int id){
        try {
            Optional<Ward> findWard = wardRepository.findById(id);
            if(findWard.isPresent()){
                wardRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } 
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
