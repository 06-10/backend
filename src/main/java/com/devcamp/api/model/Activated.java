package com.devcamp.api.model;

public enum Activated {
    Y,
    N
}
