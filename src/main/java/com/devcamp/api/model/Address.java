package com.devcamp.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "address_map")
public class Address {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull(message = "Địa chỉ không được để trống")
    private String address;

    @NotNull(message = "Vĩ độ không được để trống")
    @Column(name ="_lat")
    private double lat;

    @NotNull(message = "Kinh độ không được để trống")
    @Column(name ="_lng")
    private double lng;

    public Address() {
    }

    public Address(int id, @NotNull(message = "Địa chỉ không được để trống") String address,
            @NotNull(message = "Vĩ độ không được để trống") double lat,
            @NotNull(message = "Kinh độ không được để trống") double lng) {
        this.id = id;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    

}
