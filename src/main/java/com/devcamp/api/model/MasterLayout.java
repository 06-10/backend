package com.devcamp.api.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="master_layout")
public class MasterLayout {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    private String description;
    
    @Column(name = "project_id")
    private Integer projectId;
    
    private BigDecimal acreage ;

    @Column(name = "apartment_list")
    private String apartmentList ;

    private String photo;

    @Column(name ="date_create")
    private Date dateCreate;

    @Column(name = "date_update")
    private Date dateUpdate;

    public MasterLayout() {

    }

    public MasterLayout(Integer id, String name, String description, Integer projectId, BigDecimal acreage,
            String apartmentList, String photo, Date dateCreate, Date dateUpdate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.acreage = acreage;
        this.apartmentList = apartmentList;
        this.photo = photo;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public String getApartmentList() {
        return apartmentList;
    }

    public void setApartmentList(String apartmentList) {
        this.apartmentList = apartmentList;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    
    
}   
