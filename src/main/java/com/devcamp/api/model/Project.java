package com.devcamp.api.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "project")
public class Project {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name ="_province_id")
    private Integer provinceId;

    @Column(name = "_district_id")
    private Integer districtId;

    @Column(name = "_ward_id")
    private Integer wardId;

    @Column(name = "_street_id")
    private Integer streetId;

    @Column(name = "_name")
    private String name;
    
    private String address;
    private String description;
    private BigDecimal acreage;

    @Column(name = "construct_area")
    private BigDecimal constructArea;

    @Column(name = "num_block")
    private Integer numBlock;

    @Column(name = "num_floors")
    private String numFloors;

    @Column(name = "num_apartment")
    private Integer numApartment;
    private Boolean accepted;
    private String slogan;

    @Column(name = "apartmentt_area")
    private String apartmentArea;
    private Integer investor;

    @Column(name = "construction_contractor")
    private Integer constructionContractor;

    @Column(name = "design_unit")
    private Integer designUnit;
    private String utilities;

    @Column(name = "region_link")
    private String regionLink;
    private String photo;

    @Column(name = "_lat")
    private Double Lat;

    @Column(name = "_lng")
    private Double Lng;

    public Project() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public Integer getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(Integer numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public Integer getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(Integer numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmentArea() {
        return apartmentArea;
    }

    public void setApartmentArea(String apartmentArea) {
        this.apartmentArea = apartmentArea;
    }

    public Integer getInvestor() {
        return investor;
    }

    public void setInvestor(Integer investor) {
        this.investor = investor;
    }

    public Integer getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(Integer constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public Integer getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(Integer designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double lat) {
        Lat = lat;
    }

    public Double getLng() {
        return Lng;
    }

    public void setLng(Double lng) {
        Lng = lng;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    
    
}