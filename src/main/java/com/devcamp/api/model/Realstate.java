package com.devcamp.api.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "realestate")
public class Realstate {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String title;
    private Integer type;
    private Integer request;
    private Integer province_id;
    private Integer district_id;
    private Integer wards_id;
    private Integer street_id;
    private Integer project_id;
    private String address;
    private Integer customer_id;
    private Long price;
    private Integer price_min;
    private Integer price_time;
    private Date date_create;
    private BigDecimal acreage;
    private Integer direction;
    private Integer total_floors;
    private Integer number_floors;
    private Integer bath;
    private String apart_code;
    private BigDecimal wall_area;
    private Integer bedroom;
    private Integer balcony;
    private String landscape_view;
    private Integer apart_loca;
    private Integer apart_type;
    private Integer furniture_type;
    private Integer price_rent;
    private Double return_rate;
    private Integer legal_doc;
    private String description;
    private Integer width_y;
    private Integer long_x;
    private Integer street_house;
    private Integer FSBO;
    private Integer view_num;
    private Integer create_by;
    private Integer update_by;
    private String shape;
    private Integer distance2facade;
    private Integer adjacent_facade_num;
    private String adjacent_road;
    private Integer alley_min_width;
    private Integer adjacent_alley_min_width;
    private Integer factor;
    private String structure;
    private Integer DTSXD;
    private Integer CLCL;
    private Integer CTXD_price;
    private Integer CTXD_value;
    private String photo;
    private Double _lat;
    private Double _lng;
    
    public Realstate() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Integer getProvince_id() {
        return province_id;
    }

    public void setProvince_id(Integer province_id) {
        this.province_id = province_id;
    }

    public Integer getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(Integer district_id) {
        this.district_id = district_id;
    }

    public Integer getWards_id() {
        return wards_id;
    }

    public void setWards_id(Integer wards_id) {
        this.wards_id = wards_id;
    }

    public Integer getStreet_id() {
        return street_id;
    }

    public void setStreet_id(Integer street_id) {
        this.street_id = street_id;
    }

    public Integer getProject_id() {
        return project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getPrice_min() {
        return price_min;
    }

    public void setPrice_min(Integer price_min) {
        this.price_min = price_min;
    }

    public Integer getPrice_time() {
        return price_time;
    }

    public void setPrice_time(Integer price_time) {
        this.price_time = price_time;
    }

    public Date getDate_create() {
        return date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getTotal_floors() {
        return total_floors;
    }

    public void setTotal_floors(Integer total_floors) {
        this.total_floors = total_floors;
    }

    public Integer getNumber_floors() {
        return number_floors;
    }

    public void setNumber_floors(Integer number_floors) {
        this.number_floors = number_floors;
    }

    public Integer getBath() {
        return bath;
    }

    public void setBath(Integer bath) {
        this.bath = bath;
    }

    public String getApart_code() {
        return apart_code;
    }

    public void setApart_code(String apart_code) {
        this.apart_code = apart_code;
    }

    public BigDecimal getWall_area() {
        return wall_area;
    }

    public void setWall_area(BigDecimal wall_area) {
        this.wall_area = wall_area;
    }

    public Integer getBedroom() {
        return bedroom;
    }

    public void setBedroom(Integer bedroom) {
        this.bedroom = bedroom;
    }

    public Integer getBalcony() {
        return balcony;
    }

    public void setBalcony(Integer balcony) {
        this.balcony = balcony;
    }

    public String getLandscape_view() {
        return landscape_view;
    }

    public void setLandscape_view(String landscape_view) {
        this.landscape_view = landscape_view;
    }

    public Integer getApart_loca() {
        return apart_loca;
    }

    public void setApart_loca(Integer apart_loca) {
        this.apart_loca = apart_loca;
    }

    public Integer getApart_type() {
        return apart_type;
    }

    public void setApart_type(Integer apart_type) {
        this.apart_type = apart_type;
    }

    public Integer getFurniture_type() {
        return furniture_type;
    }

    public void setFurniture_type(Integer furniture_type) {
        this.furniture_type = furniture_type;
    }

    public Integer getPrice_rent() {
        return price_rent;
    }

    public void setPrice_rent(Integer price_rent) {
        this.price_rent = price_rent;
    }

    public Double getReturn_rate() {
        return return_rate;
    }

    public void setReturn_rate(Double return_rate) {
        this.return_rate = return_rate;
    }

    public Integer getLegal_doc() {
        return legal_doc;
    }

    public void setLegal_doc(Integer legal_doc) {
        this.legal_doc = legal_doc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWidth_y() {
        return width_y;
    }

    public void setWidth_y(Integer width_y) {
        this.width_y = width_y;
    }

    public Integer getLong_x() {
        return long_x;
    }

    public void setLong_x(Integer long_x) {
        this.long_x = long_x;
    }

    public Integer getStreet_house() {
        return street_house;
    }

    public void setStreet_house(Integer street_house) {
        this.street_house = street_house;
    }

    public Integer getFSBO() {
        return FSBO;
    }

    public void setFSBO(Integer fSBO) {
        FSBO = fSBO;
    }

    public Integer getView_num() {
        return view_num;
    }

    public void setView_num(Integer view_num) {
        this.view_num = view_num;
    }

    public Integer getCreate_by() {
        return create_by;
    }

    public void setCreate_by(Integer create_by) {
        this.create_by = create_by;
    }

    public Integer getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(Integer update_by) {
        this.update_by = update_by;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Integer getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(Integer distance2facade) {
        this.distance2facade = distance2facade;
    }

    public Integer getAdjacent_facade_num() {
        return adjacent_facade_num;
    }

    public void setAdjacent_facade_num(Integer adjacent_facade_num) {
        this.adjacent_facade_num = adjacent_facade_num;
    }

    public String getAdjacent_road() {
        return adjacent_road;
    }

    public void setAdjacent_road(String adjacent_road) {
        this.adjacent_road = adjacent_road;
    }

    public Integer getAlley_min_width() {
        return alley_min_width;
    }

    public void setAlley_min_width(Integer alley_min_width) {
        this.alley_min_width = alley_min_width;
    }

    public Integer getAdjacent_alley_min_width() {
        return adjacent_alley_min_width;
    }

    public void setAdjacent_alley_min_width(Integer adjacent_alley_min_width) {
        this.adjacent_alley_min_width = adjacent_alley_min_width;
    }

    public Integer getFactor() {
        return factor;
    }

    public void setFactor(Integer factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Integer getDTSXD() {
        return DTSXD;
    }

    public void setDTSXD(Integer dTSXD) {
        DTSXD = dTSXD;
    }

    public Integer getCLCL() {
        return CLCL;
    }

    public void setCLCL(Integer cLCL) {
        CLCL = cLCL;
    }

    public Integer getCTXD_price() {
        return CTXD_price;
    }

    public void setCTXD_price(Integer cTXD_price) {
        CTXD_price = cTXD_price;
    }

    public Integer getCTXD_value() {
        return CTXD_value;
    }

    public void setCTXD_value(Integer cTXD_value) {
        CTXD_value = cTXD_value;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double get_lat() {
        return _lat;
    }

    public void set_lat(Double _lat) {
        this._lat = _lat;
    }

    public Double get_lng() {
        return _lng;
    }

    public void set_lng(Double _lng) {
        this._lng = _lng;
    }

    
}
