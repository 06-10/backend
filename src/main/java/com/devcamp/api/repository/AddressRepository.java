package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.api.model.Address;

public interface AddressRepository extends JpaRepository<Address,Integer>{
    
}
