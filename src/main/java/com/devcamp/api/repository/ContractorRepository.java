package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.api.model.Contractor;

public interface ContractorRepository extends JpaRepository<Contractor,Integer> {
    
}
