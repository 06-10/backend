package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.api.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Integer> {
    
}
