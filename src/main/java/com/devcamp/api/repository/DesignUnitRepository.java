package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.api.model.DesignUnit;

public interface DesignUnitRepository extends JpaRepository<DesignUnit,Integer>{
    
}
