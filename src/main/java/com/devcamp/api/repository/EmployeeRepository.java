package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
    
}
