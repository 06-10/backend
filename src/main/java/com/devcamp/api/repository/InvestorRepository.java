package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Investor;

public interface InvestorRepository extends  JpaRepository<Investor,Integer>{
    
}
