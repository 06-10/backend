package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Location;

public interface LocationRepository extends JpaRepository<Location,Integer>{
    
}
