package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.MasterLayout;

public interface MasterLayoutRepository extends JpaRepository<MasterLayout,Integer>{
    
}
