package com.devcamp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.api.model.Project;

public interface ProjectRepository extends JpaRepository<Project,Integer>{
    
    List<Project> findByProvinceId(int id);
    
}
