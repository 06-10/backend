package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Province;

public interface ProvinceRepository extends JpaRepository<Province,Integer> {
    
}
