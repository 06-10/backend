package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Realstate;

public interface RealStateRepository extends JpaRepository<Realstate,Integer> {
    
}
