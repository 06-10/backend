package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.RegionLink;

public interface RegionLinkRepository extends JpaRepository<RegionLink,Integer>{
    
}
