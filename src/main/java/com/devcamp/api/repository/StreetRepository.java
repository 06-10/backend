package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Street;

public interface StreetRepository extends JpaRepository<Street,Integer>{
    
}
