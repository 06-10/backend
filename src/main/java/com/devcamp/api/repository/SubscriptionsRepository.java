package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Subscriptions;

public interface SubscriptionsRepository  extends JpaRepository<Subscriptions,Integer> {
    
}
