package com.devcamp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.model.Ward;

public interface WardRepository extends JpaRepository<Ward,Integer>{
    
}
